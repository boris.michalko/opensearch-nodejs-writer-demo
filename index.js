const { createIndex, indexDocument, generateLogEntry } = require('./opensearch-logger');
const { Client } = require('@opensearch-project/opensearch');

const argv = process.argv.slice(2);
const OPENSEARCH_URL = argv[0] || (process.env.OPENSEARCH_URL || 'http://localhost:9200');
const client = new Client({ node: OPENSEARCH_URL });

(async () => {
  const indexName = 'my_index';
  await createIndex(client, indexName);

  setInterval(async () => {
    const logEntry = generateLogEntry();
    await indexDocument(client, indexName, logEntry);
  }, 5000);
})();
