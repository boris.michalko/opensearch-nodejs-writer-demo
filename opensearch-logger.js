const crypto = require('crypto');

function randomString(length) {
  return crypto.randomBytes(length).toString('hex');
}

function generateLogEntry() {
  const content = randomString(32);
  const timestamp = new Date().toISOString();
  return {
    content,
    timestamp,
  };
}

async function createIndex(client, indexName) {
  try {
    const indexExists = await client.indices.exists({ index: indexName });

    if (!indexExists) {
      const { body } = await client.indices.create({
        index: indexName,
      });
      console.log(`Index '${indexName}' created.`, body);
    } else {
      console.log(`Index '${indexName}' already exists.`);
    }
  } catch (error) {
    console.error(`Error while checking/creating index: ${indexName}`, error);
  }
}

async function indexDocument(client, indexName, document) {
  try {
    const { body } = await client.index({
      index: indexName,
      body: document,
    });
    console.log(body);
  } catch (error) {
    console.error(`Error indexing document in index: ${indexName}`, error);
  }
}

module.exports = {
  randomString,
  generateLogEntry,
  createIndex,
  indexDocument,
};
