const {
  randomString,
  generateLogEntry,
  createIndex,
  indexDocument,
} = require('./opensearch-logger');

describe('randomString', () => {
  it('should return a string of specified length', () => {
    const length = 20;
    const result = randomString(length);
    expect(result).toHaveLength(length * 2);
  });

  it('should return a different string each time it is called', () => {
    const length = 20;
    const result1 = randomString(length);
    const result2 = randomString(length);
    expect(result1).not.toEqual(result2);
  });
});

describe('generateLogEntry', () => {
  it('should return an object with content and timestamp', () => {
    const result = generateLogEntry();
    expect(result).toHaveProperty('content');
    expect(result).toHaveProperty('timestamp');
  });
});
